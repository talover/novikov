var novikov = {};

jQuery(document).ready(function ($) {

	$(".home-loader").addClass("active");

	novikov.loading = new NovikovLoading();

	if (typeof points != "undefined")
		novikov.map = new NovikovMap(points);

	$(window).on("load", function () {

		novikov.loading.startLoading(function () {
			$(".first-slide").addClass("intro-active");

			setTimeout(function () {
				$(".top-bar").addClass("fadeInDown");
				$(".social-icons").addClass("fadeInUp");
			}, 1200);
		});

		$(".btn-nav").on("click", function () {

			var btnActive = $(this).hasClass("btn_nav_active");
			var menuActive = $('.top-bar').find('nav').hasClass('active-nav');

			if (btnActive && menuActive) {
				$('body').removeClass('active-popup');
				$('.top-bar').find('nav').removeClass('active-nav');
			}

			if (!btnActive && !menuActive) {
				$('body').addClass('active-popup');
				$('.top-bar').find('nav').addClass('active-nav');
			}

			if (btnActive && !menuActive) {
				$('body').removeClass('active-popup');
				$(".form-popup").removeClass("active");
			}

			$(this).toggleClass('btn_nav_active');


		});

		$(".header-popup-btn").on("click", function (e) {
			e.preventDefault();

			$('body').addClass('active-popup');
			$(".form-popup").addClass("active");
			$(this).addClass('btn_nav_active');
			$('.btn-nav').addClass('btn_nav_active');
			$('header').find('nav').removeClass('active-nav');
		})

		var scroll = new LocomotiveScroll({
			el: document.querySelector('#container'),
			smooth: true
		});

		scroll.on("scroll", function (e) {
			if (e.scroll.y >= 20) {
				$(".top-bar").addClass("fixed");
			} else {
				$(".top-bar").removeClass("fixed");
			}
		});

		$(".scroll-block-fancy").mCustomScrollbar();

	});

});

var NovikovMap = (function (points) {
	var self = this;

	var $_map = $('#map-wrapper');
	var _map;
	var _type = "wedding";

	var _createMap = (function () {

		_map = new jvm.Map({
			container: $_map,
			map: 'europe_mill',
			backgroundColor: 'white',
			scale: 'red',
			zoomOnScroll: false,
			regionStyle: {
				initial: {
					fill: '#cecece'
				}
			},
			onMarkerTipShow: function (e, label, code) {
			},
			onMarkerClick: function (e, code) {
				_showEventList(_type, code);
			}
		});

	});

	var _drawPoints = (function (_type) {
		_map.removeAllMarkers();
		_map.addMarkers(points[_type]);
	});

	var _hideEventsList = function () {
		$('[data-code]').removeClass("hover");

		$(".block-fency").removeClass("active");
	};

	var _showEventList = function (_type, point) {

		console.log(".event-type-" + _type + ".point-" + point);
		console.log($(".event-type-" + _type + ".point-" + point));

		$(".event-type-" + _type + ".point-" + point).addClass("active");

		$('[data-code="' + points[_type][point].code + '"]').addClass("hover");
	};

	var _binds = (function () {
		$(document).on("click", ".map-point-type-change", function () {

			$(".map-point-type-change").removeClass("active");
			$(this).addClass("active");

			_hideEventsList();
			_type = $(this).attr("data-type");
			_drawPoints(_type);
		});

		$(document).on("click", ".close-button-fancy", function () {
			_hideEventsList();
		});

		$_map.on("mouseenter", ".jvectormap-marker", function () {
			var i = $(this).attr("data-index");

			$('[data-code="' + points[_type][i].code + '"]').addClass("hover");
		});

		$_map.on("mouseleave", ".jvectormap-marker", function () {
			var i = $(this).attr("data-index");

			$('[data-code="' + points[_type][i].code + '"]').removeClass("hover");
		});
	});

	var __construct = (function () {
		_binds();
		_createMap();

		_drawPoints(_type);
	})();

});

var NovikovLoading = (function () {

	var self = this;
	var $_loaderWrapper = $(".loader-wrapper");
	var _animTime = 1200;
	var _animTimer = null;
	var _timer = null;

	self.hideLoader = (function (callback) {
		if (_animTimer) {
			clearTimeout(_animTimer);
		}

		$_loaderWrapper.removeClass("hide");
		$_loaderWrapper.addClass("anim");
		$_loaderWrapper.addClass("hide-to-right");

		setTimeout(function () {
			$_loaderWrapper.removeClass("anim");
			$_loaderWrapper.removeClass("hide-to-right");
			$_loaderWrapper.removeClass("show-from-left");
			$_loaderWrapper.addClass("hide");

			setTimeout(function () {
				$(".hide-loading").removeClass("hide-loading");
			}, 2000);

			if (typeof callback !== "undefined") {
				callback();
			}
		}, _animTime);

	});

	self.showLoader = (function (callback) {
		if (_animTimer) {
			clearTimeout(_animTimer);
		}

		$_loaderWrapper.removeClass("hide");
		$_loaderWrapper.addClass("anim");
		$_loaderWrapper.addClass("show-from-left");

		_animTimer = setTimeout(function () {
			if (typeof callback !== "undefined") {
				callback();
			}
		}, _animTime);
	});

	self.startLoading = (function (callback) {

		var $images = $(".wait-for-load");

		if ($images.length) {

			var i = $images.length;

			$images.each(function (index, value) {
				value.src = $(value).attr("data-src");

				value.onload = function () {
					i--;
					check();
				};
			});

		} else {
			self.hideLoader(callback);
		}

		var check = (function () {
			if (i === 0) {
				self.hideLoader(callback);
			}
		});


	});

	var _checkIfLink = (function (link) {
		return link.indexOf(window.baseUrl) !== -1;
	});

	var _binds = (function () {
		$(document).on("click", "a", function (e) {

			var link = $(this).attr("href");

			if (!$(this).hasClass("not-a-link") && typeof link !== "undefined" && link && _checkIfLink(link)) {
				e.preventDefault();

				self.showLoader((function () {
					window.location.href = link;
				}));

			}

		})
	});

	var __construct = (function () {
		_binds();
	})();
});
